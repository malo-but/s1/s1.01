/**
 * @author Malo Massieu--Rocabois INFO1D1
 * @version 2.0
 * Grundy game.
 */

import java.util.Arrays;

class Grundy1 {

	// Entry point of the program
	void principal() {

		// Tests all methods
		testAll();

		System.out.println("\nWelcome to the Grundy game");

		int numberOfMatches = 0;
		// We can't play with less than 3 matches
		while (numberOfMatches < 3) {
			// We ask the user how many matches he wants to play with
			numberOfMatches = SimpleInput.getInt("Number of matches : ");
		}

		// We start the game
		game(numberOfMatches);
	}

	/**
	 * Initialization and main game loop
	 */
	void game(int numberOfMatches) {

		/**
		 * We create an array of size (numberOfMatches - 1)
		 * as the maximum number of lines for n matches is (n - 1)
		 * 
		 * matchesTab.length is the number of lines
		 * matchesTab[i] is the number of matches on line i
		 */
		int[] matchesTab = new int[numberOfMatches - 1];

		// We initialize the first line with the total number of matches
		matchesTab[0] = numberOfMatches;

		// We ask the name of the different players
		String player1 = SimpleInput.getString("Name of the first player : ");
		String player2 = "";
		// We can't have two players with the same name
		do {
			player2 = SimpleInput.getString("Name of the second player : ");
		} while (player2.equals(player1));
		// We set the current player to player2
		String currentPlayer = player2;

		// We initialize the line and number of matches to 0
		int line = 0;
		int matchesToPlay = 0;

		// Main game loop
		while (!isGameOver(matchesTab)) {
			
			// We switch the current player
			currentPlayer = switchPlayer(currentPlayer, player1, player2);

			// We display the matches table
			displayMatches(matchesTab);
	
			// We display the current player
			System.out.println("Playing now : " + currentPlayer);

			// If there is only one playable line then it is selected by default
			if (getNumberOfLegalLines(matchesTab) == 1) {
				line = getOnlyLegalLine(matchesTab);
			} else {
				// We ask the current player on which line he wants to play
				do {
					line = SimpleInput.getInt("Line : ");
				} while (!isLegalLine(matchesTab, line));
			}
			
			// We ask the current player how many matches he wants to play with
			do {
				matchesToPlay = SimpleInput.getInt("Number of matches to" + 
				  " separate from line " + line + " : ");
			} while (!isLegalNumberofMatches(matchesTab, line, matchesToPlay));
			
			// We update the matches table with the player's choice
			updateMatches(matchesTab, line, matchesToPlay);
		}

		// We display the winner when the game is finished
		System.out.println(currentPlayer + " has won the game!");
	}

	/**
	 * Tells is the game is over
	 * @param matchesTab The matches table
	 * @return true if the game is over
	 */
	boolean isGameOver(int[] matchesTab) {
		// If no line can be played, the game is over
		return getNumberOfLegalLines(matchesTab) <= 0;
	}

	/**
	 * Switches the player
	 * @param currentPlayer Current player
	 * @param player1 First player
	 * @param player2 Second player
	 * @return Next player
	 */
	String switchPlayer(String currentPlayer, String player1, String player2) {

		// If the current player is player1 then we return player2
		String res = player1;
		if (currentPlayer == player1) {
			res = player2;
		}
		return res;
	}

	/**
	 * Displays the matches
	 * @param matchesTab Array of matches
	 */
	void displayMatches(int[] matchesTab) {
		System.out.println();

		// We get the number of lines
		int lines = getNumberOfActiveLines(matchesTab);
		
		// We loop through the lines
		for (int i = 0; i < lines; i++) {;
			System.out.print(i + " :");

			// We loop through the matches
			for (int j = 0; j < matchesTab[i]; j++) {

				// We display the match
				System.out.print(" | ");
			}

			System.out.println();
		}

		System.out.println();
	}

	/**
	 * Returns the number of legal lines
	 * @param matchesTab Array of matches
	 * @return Number of legal lines
	 */
	int getNumberOfLegalLines(int[] matchesTab) {
		int res = 0;
		int i = 0;

		// We loop through each line
		while (i < matchesTab.length) {

			// We check if the line can be played
			if (isLegalLine(matchesTab, i)) {

				// If it can we increment by one the res variable
				res++;
			}
			i++;
		}

		return res;
	}


	boolean isLegalLine(int[] matchesTab, int line) {
		boolean res = true;

		// We check if the chosen line is between 0 and the number of
		// active lines both included
		if (line >= getNumberOfActiveLines(matchesTab) || line < 0) {
			res = false;
		// We check if there are at least 3 matches on the line
		} else if (matchesTab[line] < 3) {
			res = false;
		}

		return res;
	}

	/**
	 * Returns the first legal line if there are multiple
	 * and -1 if there are none
	 * @param matchesTab Array of matches
	 * @return Only legal line
	 */
	int getOnlyLegalLine(int[] matchesTab) {

		// We initialize res at -1 in case there isn't any legal line
		int res = -1;
		boolean stop = false;
		int i = 0;
		
		// We loop through the lines and we stop if we find a legal one
		while (i < matchesTab.length && !stop) {

			// We check if the current line can be played
			if (isLegalLine(matchesTab, i)) {

				// If it can we assign its index to the res variable
				res = i;
				// And we stop the loop
				stop = true;
			}
			i++;
		}

		return res;
	}

	/**
	 * Returns the number of active lines
	 * @param matchesTab Array of matches
	 * @return Number of active lines
	 */
	int getNumberOfActiveLines(int[] matchesTab) {
		int res = 0;
		boolean stop = false;
		int i = 0;

		// We loop through the lines and we stop if we find an empty one
		// as there can't be an empty line between two active ones
		while (i < matchesTab.length && !stop) {

			// We check if the line isn't empty
			if (matchesTab[i] != 0) {
				res++;

			// Otherwise we stop the loop
			} else {
				stop = true;
			}
			i++;
		}
		return res;
	}

	/**
	 * Checks if the number of matches to play is legal
	 * @param matchesTab Array of matches
	 * @param line Line to play with
	 * @param matches Number of matches to play with
	 * @return True if the number of matches to play is legal
	 */
	boolean isLegalNumberofMatches(int[] matchesTab, int line, int matches) {
		boolean res = true;
		// We get the number of matches on the chosen line
		int nbMatchesOnLIne = matchesTab[line];

		// We check if the chosen number of matches is between 1 
		// and the number of matches on the line both included
		if (matches >= nbMatchesOnLIne || matches < 1) {
			res = false;

		// We check if the number of matches on the line is a multiple of 2
		} else if (nbMatchesOnLIne % 2 == 0) {
			// We check if the chosen number of matches is half of
			// the number of matches on the line
			if (nbMatchesOnLIne - matches == nbMatchesOnLIne / 2) {
				res = false;
			}
		}

		return res;
	}

	/**
	 * Updates the matches table
	 * @param matchesTab Array of matches
	 * @param line Line to play with
	 * @param matches Number of matches to play with
	 */
	void updateMatches(int[] matchesTab, int line, int matches) {

		// We save the number of matches on the chosen line
		int temp = matchesTab[line];

		// We update the number of matches on the chosen line
		matchesTab[line] = matches;

		// We put the difference between the number of matches on the last line
		matchesTab[getNumberOfActiveLines(matchesTab)] = temp - matches;
	}

	// !! TEST METHODS !!

	/**
	 * Tests all methods
	 */
	void testAll() {
		testGame();
		testIsGameOver();
		testSwitchPlayer();
		testDisplayMatches();
		testGetNumberOfLegalLines();
		testIsLegalLine();
		testGetOnlyLegalLine();
		testGetNumberOfActiveLines();
		testIsLegalNumberofMatches();
		testUpdateMatches();
	}

	/**
	 * Tests the game() method
	 */
	void testGame() {
		System.out.println("\n*** testGame() ***");

		System.out.println("\nEnter any name you want for the first player.");
		System.out.println("Enter any name you want for the second player" +
		  " as long as it is different from the first's.");
		System.out.println("Enter 1 when asked for the number of matches to play with.");
		System.out.println("The first player should win.\n");

		game(3);

		System.out.println();
	}

	void testIsGameOver() {
		System.out.print("\n*** testIsGameOver() ***");

		testCaseIsGameOver(new int[] {4, 2}, false);
		testCaseIsGameOver(new int[] {4, 0}, false);
		testCaseIsGameOver(new int[] {0, 0}, true);
		testCaseIsGameOver(new int[] {}, true);

		System.out.println();
	}

	void testCaseIsGameOver(int[] matchesTab, boolean expected) {
		// Act
		boolean res = isGameOver(matchesTab);

		// Arrange
		System.out.print("\nisGameOver(" + Arrays.toString(matchesTab) + ") = " + res);

		// Assert
		if (res == expected) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	/**
	 * Tests the switchPlayer() method
	 */
	void testSwitchPlayer() {
		System.out.println("\n*** testSwitchPlayer() ***");

		testCaseSwitchPlayer("current", "current", "other", "other");
		testCaseSwitchPlayer("current", "other", "current", "other");
		testCaseSwitchPlayer("current", "current", "current", "current");
		testCaseSwitchPlayer("", "other", "", "other");
		testCaseSwitchPlayer("", "", "", "");

		System.out.println();
	}

	/**
	 * Tests a call to the switchPlayer() method
	 * @param player1 First player
	 * @param player2 Second player
	 * @param currentPlayer Current player
	 * @param expected Expected result
	 */
	void testCaseSwitchPlayer(String currentPlayer, String player1,
	  String player2, String expected) {

		// Act
		String res = switchPlayer(currentPlayer, player1, player2);

		// Arrange
		System.out.print("\nswitchPlayer(\"" + currentPlayer + "\", \"" +
		  player1 + "\", \"" + player2 + "\") = " + res);

		// Assert
		if (res == expected) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	/**
	 * Tests the displayMatches() method
	 */
	void testDisplayMatches() {
		System.out.println("\n*** testDisplayMatches() ***");

		int[] matchesTab1 = {1, 2, 3, 0};
		// Should display 3 lines with respectively 1, 2 and 3 matches
		testCaseDisplayMatches(matchesTab1);

		int[] matchesTab2 = {0, 2, 3, 0};
		// Shouldn't display anything because the first line is empty
		testCaseDisplayMatches(matchesTab2);

		int[] matchesTab3 = {1, 2, 3, 1};
		// Should display 4 lines with respectively 1, 2, 3 and 1 matches
		testCaseDisplayMatches(matchesTab3);

		int[] matchesTab4 = {};
		// Shouldn't display anything because the array is empty
		testCaseDisplayMatches(matchesTab4);

		System.out.println();
	}

	/**
	 * Tests a call to the displayMatches() method
	 * @param matchesTab Array of matches
	 */
	void testCaseDisplayMatches(int[] matchesTab) {

		// Arrange
		System.out.print("\ndisplayMatches(" + Arrays.toString(matchesTab) +
		  ") =\n\t\t");

		// Act
		displayMatches(matchesTab);

	}

	/**
	 * Tests the getNumberOfLegalLines() method
	 */
	void testGetNumberOfLegalLines() {
		System.out.print("\n*** testGetNumberOfLegalLines() ***");

		int[] matchesTab1 = {5, 3, 2, 1, 0};
		testCaseGetNumberOfLegalLines(matchesTab1, 2);

		int[] matchesTab2 = {1, 1, 2, 1, 4};
		testCaseGetNumberOfLegalLines(matchesTab2, 1);

		int[] matchesTab3 = {2, 1, 2, 1, 1};
		testCaseGetNumberOfLegalLines(matchesTab3, 0);

		int[] matchesTab4 = {0, 0, 0, 0, 0};
		testCaseGetNumberOfLegalLines(matchesTab4, 0);

		System.out.println();
	}

	/**
	 * Tests a call to the getNumberOfLegalLines() method
	 * @param matchesTab Array of matches
	 * @param expected Expected result
	 */
	void testCaseGetNumberOfLegalLines(int[] matchesTab, int expected) {

		// Act
		int res = getNumberOfLegalLines(matchesTab);

		// Arrange
		System.out.print("\ngetNumberOfLegalLines(" +
		  Arrays.toString(matchesTab) + ") = " + res);

		// Assert
		if (res == expected) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	/**
	 * Tests the isLegalLine() method
	 */
	void testIsLegalLine() {
		System.out.print("\n*** testIsLegalLine() ***");

		int[] matchesTab1 = {5, 3, 2, 1, 0};
		testCaseIsLegalLine(matchesTab1, 0, true);
		testCaseIsLegalLine(matchesTab1, 1, true);
		testCaseIsLegalLine(matchesTab1, 2, false);
		testCaseIsLegalLine(matchesTab1, 3, false);
		testCaseIsLegalLine(matchesTab1, 4, false);

		int[] matchesTab2 = {1, 1, 2, 1, 4};
		testCaseIsLegalLine(matchesTab2, 0, false);
		testCaseIsLegalLine(matchesTab2, 1, false);
		testCaseIsLegalLine(matchesTab2, 2, false);
		testCaseIsLegalLine(matchesTab2, 3, false);
		testCaseIsLegalLine(matchesTab2, 4, true);

		int[] matchesTab3 = {2, 1, 2, 1, 1};
		testCaseIsLegalLine(matchesTab3, 0, false);
		testCaseIsLegalLine(matchesTab3, 1, false);
		testCaseIsLegalLine(matchesTab3, 2, false);
		testCaseIsLegalLine(matchesTab3, 3, false);
		testCaseIsLegalLine(matchesTab3, 4, false);

		System.out.println();
	}

	/**
	 * Tests a call to the isLegalLine() method
	 * @param matchesTab Array of matches
	 * @param line Line number
	 * @param expected Expected result
	 */
	void testCaseIsLegalLine(int[] matchesTab, int line, boolean expected) {

		// Act
		boolean res = isLegalLine(matchesTab, line);

		// Arrange
		System.out.print("\nisLegalLine(" + Arrays.toString(matchesTab) +
		  ", " + line + ") = " + res);

		// Assert
		if (res == expected) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	/**
	 * Tests the getOnlyLegalLine() method
	 */
	void testGetOnlyLegalLine() {
		System.out.print("\n*** testGetOnlyLegalLine() ***");

		int[] matchesTab1 = {5, 2, 2, 1, 0};
		testCaseGetOnlyLegalLine(matchesTab1, 0);

		int[] matchesTab2 = {1, 1, 2, 1, 4};
		testCaseGetOnlyLegalLine(matchesTab2, 4);

		int[] matchesTab3 = {2, 1, 2, 1, 1};
		testCaseGetOnlyLegalLine(matchesTab3, -1);

		int[] matchesTab4 = {2, 3, 2, 4, 1};
		testCaseGetOnlyLegalLine(matchesTab4, 1);

		System.out.println();
	}

	/**
	 * Tests a call to the getOnlyLegalLine() method
	 * @param matchesTab Array of matches
	 * @param expected Expected result
	 */
	void testCaseGetOnlyLegalLine(int[] matchesTab, int expected) {

		// Act
		int res = getOnlyLegalLine(matchesTab);

		// Arrange
		System.out.print("\ngetOnlyLegalLine(" +
		  Arrays.toString(matchesTab) + ") = " + res);

		// Assert
		if (res == expected) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	/**
	 * Tests the getNumberOfMatches() method
	 */
	void testGetNumberOfActiveLines() {
		System.out.print("\n*** testGetNumberOfActiveLines() ***");

		int[] matchesTab1 = {5, 3, 2, 1, 0};
		testCaseGetNumberOfActiveLines(matchesTab1, 4);

		int[] matchesTab2 = {1, 1, 2, 1, 4};
		testCaseGetNumberOfActiveLines(matchesTab2, 5);

		int[] matchesTab3 = {2, 1, 2, 1, 1};
		testCaseGetNumberOfActiveLines(matchesTab3, 5);

		int[] matchesTab4 = {0, 0};
		testCaseGetNumberOfActiveLines(matchesTab4, 0);

		int[] matchesTab5 = {};
		testCaseGetNumberOfActiveLines(matchesTab5, 0);

		System.out.println();
	}

	/**
	 * Tests a call to the getNumberOfActiveLines() method
	 * @param matchesTab Array of matches
	 * @param expected Expected result
	 */
	void testCaseGetNumberOfActiveLines(int[] matchesTab, int expected) {

		// Act
		int res = getNumberOfActiveLines(matchesTab);

		// Arrange
		System.out.print("\ngetNumberOfActiveLines(" +
		  Arrays.toString(matchesTab) + ") = " + res);

		// Assert
		if (res == expected) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	/**
	 * Tests the getNumberOfMatches() method
	 */
	void testIsLegalNumberofMatches() {
		System.out.print("\n*** testIsLegalNumberofMatches() ***");

		int[] matchesTab1 = {5, 3, 2, 1, 0};
		testCaseIsLegalNumberofMatches(matchesTab1, 0, 2, true);
		testCaseIsLegalNumberofMatches(matchesTab1, 2, 2, false);
		testCaseIsLegalNumberofMatches(matchesTab1, 1, 3, false);


		int[] matchesTab2 = {1, 1, 2, 1, 4};
		testCaseIsLegalNumberofMatches(matchesTab2, 0, 1, false);
		testCaseIsLegalNumberofMatches(matchesTab2, 0, 2, false);
		testCaseIsLegalNumberofMatches(matchesTab2, 4, 2, false);


		System.out.println();
	}
	
	/**
	 * Tests a call to the isLegalNumberofMatches() method
	 * @param matchesTab Array of matches
	 * @param line Line number
	 * @param numberOfMatches Number of matches
	 * @param expected Expected result
	 */
	void testCaseIsLegalNumberofMatches(int[] matchesTab, int line,
	  int numberOfMatches, boolean expected) {

		// Act
		boolean res = isLegalNumberofMatches(matchesTab, line, numberOfMatches);

		// Arrange
		System.out.print("\nisLegalNumberofMatches(" +
		  Arrays.toString(matchesTab) + ", " + line + ", " + numberOfMatches +
		  ") = " + res);

		// Assert
		if (res == expected) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	/**
	 * Tests the getNumberOfMatches() method
	 */
	void testUpdateMatches() {
		System.out.print("\n*** testUpdateMatches() ***");

		int[] matchesTab1 = {5, 3, 2, 1, 0};
		testCaseUpdateMatches(matchesTab1, 0, 2, new int[] {2, 3, 2, 1, 3});
		
		int[] matchesTab2 = {1, 1, 2, 1, 4, 0};
		testCaseUpdateMatches(matchesTab2, 4, 1, new int[] {1, 1, 2, 1, 1, 3});

		int[] matchesTab3 = {2, 3, 2, 0};
		testCaseUpdateMatches(matchesTab3, 1, 1, new int[] {2, 1, 2, 2});

		System.out.println();
	}

	/**
	 * Tests a call to the updateMatches() method
	 * @param matchesTab Array of matches
	 * @param line Line number
	 * @param numberOfMatches Number of matches
	 * @param expected Expected result
	 */
	void testCaseUpdateMatches(int[] matchesTab, int line,
	  int numberOfMatches, int[] expected) {

		// Act
		updateMatches(matchesTab, line, numberOfMatches);

		// Arrange
		System.out.print("\nupdateMatches(" + Arrays.toString(matchesTab) +
		  ", " + line + ", " + numberOfMatches + ") = " +
		  Arrays.toString(matchesTab));

		// Assert
		if (Arrays.equals(matchesTab, expected)) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

}

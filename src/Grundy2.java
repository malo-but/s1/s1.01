/**
 * @author Malo Massieu--Rocabois INFO1D1
 * @version 2.0
 * Grundy game.
 */

import java.util.Arrays;

class Grundy2 {
	
	// Entry point of the program
	void principal() {
		
		// Tests all methods
		//testAll();

		System.out.println("\nWelcome to the Grundy game");

		int numberOfMatches = 0;
		// We can't play with less than 3 matches
		while (numberOfMatches < 3) {
			// We ask the user how many matches he wants to play with
			numberOfMatches = SimpleInput.getInt("Number of matches : ");
		}

		/**
		 * We create an array of size (numberOfMatches - 1)
		 * as the maximum number of lines for n matches is (n - 1)
		 * 
		 * matchesTab.length is the number of lines
		 * matchesTab[i] is the number of matches on line i
		 */
		int[] matchesTab = new int[numberOfMatches - 1];

		// We initialize the first line with the total number of matches
		matchesTab[0] = numberOfMatches;

		int gameType = -1;
		// There are only 2 game types
		while (gameType < 0 || gameType > 1) {
			// We ask the user which game type he wants to play
			gameType = SimpleInput.getInt("Game type :" +
			  "\n0. Player vs Player\n1. Player vs Computer\n> ");
		}

		// We create a new game
		if (gameType == 0) {
			// Player vs Player
			pvpGame(matchesTab);
		} else {
			// Player vs Computer
			pvcGame(matchesTab);
		}
	}

	/**
	 * Player vs Player game
	 * @param matchesTab The array of matches
	 */
	void pvpGame(int[] matchesTab) {
		
		// We ask the name of the different players
		String player1 = SimpleInput.getString("Name of the first player : ");
		String player2 = "";
		// We can't have two players with the same name
		do {
			player2 = SimpleInput.getString("Name of the second player : ");
		} while (player2.equals(player1));
		// We set the current player to player2
		String currentPlayer = player2;

		// Main game loop
		while (!isGameOver(matchesTab)) {
			
			// We switch the current player
			currentPlayer = switchPlayer(currentPlayer, player1, player2);

			// We display the matches table
			displayMatches(matchesTab);

			// We display the current player
			System.out.println("Playing now : " + currentPlayer);

			// The current player plays
			playerPlay(matchesTab);
		}

		// We display the winner when the game is finished
		System.out.println(currentPlayer + " has won the game!");
	}

	/**
	 * Player vs Computer game
	 * @param matchesTab The array of matches
	 */
	void pvcGame(int[] matchesTab) {

		String player = "";
		// The player can't have the same name as the computer
		do {
			player = SimpleInput.getString("Name of the player : ");
		} while (player.equals("CPU"));
		// We set the computer's name to CPU
		String computer = "CPU";
		// We set the current player depending on who starts
		String currentPlayer = getSecondPlayer(player, computer);

		int cpuType = -1;
		// There are only 2 game types
		while (cpuType < 0 || cpuType > 1) {
			// We ask the user which cpu type he wants to play with
			cpuType = SimpleInput.getInt("CPU type :" +
			"\n0. Random moves\n1. MiniMax algorithm\n> ");
		}

		// Main game loop
		while (!isGameOver(matchesTab)) {
			
			// We switch the current player
			currentPlayer = switchPlayer(currentPlayer, player, computer);

			// We display the matches table
			displayMatches(matchesTab);

			// We display the current player
			System.out.println("Playing now : " + currentPlayer);

			// We check which player is playing
			if (currentPlayer == player) {
				// The player is playing
				playerPlay(matchesTab);

			} else {
				// The computer is playing

				if (cpuType == 0) {
					// Random moves
					computerPlayRandom(matchesTab);
				} else {
					// MiniMax algorithm
					computerPlayMiniMax(matchesTab);
				}
				
			}

		}

		// We display the winner when the game is finished
		System.out.println(currentPlayer + " has won the game!");
	}

	/**
	 * Tells is the game is over
	 * @param matchesTab The matches table
	 * @return true if the game is over
	 */
	boolean isGameOver(int[] matchesTab) {
		// If no line can be played, the game is over
		return getNumberOfLegalLines(matchesTab) <= 0;
	}

	/**
	 * Make the current player play
	 * @param matchesTab The matches table
	 */
	void playerPlay(int[] matchesTab) {

		// We initialize the line and number of matches to 0
		int line = 0;
		int matchesToPlay = 0;

		// If there is only one playable line then it is selected by default
		if (getNumberOfLegalLines(matchesTab) == 1) {
			line = getOnlyLegalLine(matchesTab);
		} else {
			// We ask the current player on which line he wants to play
			do {
				line = SimpleInput.getInt("Line : ");
			} while (!isLegalLine(matchesTab, line));
		}
		
		// We ask the current player how many matches he wants to play with
		do {
			matchesToPlay = SimpleInput.getInt("Number of matches to" + 
				  " separate from line " + line + " : ");
		} while (!isLegalNumberofMatches(matchesTab, line, matchesToPlay));

		// We update the matches table with the player's choice
		updateMatches(matchesTab, line, matchesToPlay);
	}

	/**
	 * Makes the computer play with random moves
	 * @param matchesTab The matches table
	 */
	void computerPlayRandom(int[] matchesTab) {

		// We initialize the line and number of matches to 0
		int line = 0;
		int matches = 0;

		// If there is only one playable line then it is selected by default
		if (getNumberOfLegalLines(matchesTab) == 1) {
			line = getOnlyLegalLine(matchesTab);
		} else {
			// We select a random line
			do {
				line = (int) (Math.random() * getNumberOfActiveLines(matchesTab));
			} while (!isLegalLine(matchesTab, line));
		}

		// We select a random number of matches
		do {
			matches = (int) (Math.random() * matchesTab[line]);
		} while (!isLegalNumberofMatches(matchesTab, line, matches));

		// We display the computer's choice
		System.out.println("The computer plays " + matches +
		  " matches on line " + line + ".");

		// We update the matches table with the computer's choice
		updateMatches(matchesTab, line, matches);
	} 

	/**
	 * Makes the computer play using the minimax algorithm
	 * @param matchesTab The matches table
	 */
	void computerPlayMiniMax(int[] matchesTab) {
		
		// We create a copy of the matches table
		int[] matchesTabCopy = clone(matchesTab);

		// We get the best line and number of matches to play with
		// bestMove[0] = line and bestMove[1] = number of matches
		int[] bestmove = miniMaxPlay(matchesTabCopy);

		// We display the computer's choice
		System.out.println("The computer plays " + bestmove[1] +
		  " matches on line " + bestmove[0] + ".");

		// We update the matches table with the computer's choice
		updateMatches(matchesTab, bestmove[0], bestmove[1]);
	}

	/**
	 * Returns the best move for the computer
	 * @param matchesTab The matches table
	 * @return The best move
	 */
	int[] miniMaxPlay(int[] matchesTab) {

		// We make a copy of the matches table
		int[] matchesTabCopy = clone(matchesTab);

		// We initialize the best move to 0
		int[] bestmove = new int[2];

		// We initialize the best score to -1000
		int bestScore = -1000;

		// We get the list of the legal lines
		int[] legalLines = getListLegalLines(matchesTab);

		// We loop through the legal lines
		for (int i = 0; i < legalLines.length; i++) {

			// We get the list of the legal number of matches for the current line
			int[] legalNumberOfMatches = 
			  getListLegalNumberOfMatches(matchesTab, legalLines[i]);

			// We loop through the legal number of matches for line i
			for (int j = 0; j < legalNumberOfMatches.length; j++) {

				// We update the matches table with the current move
				updateMatches(matchesTab, legalLines[i], legalNumberOfMatches[j]);

				// We recursively get the score of the current move
				int score = miniMax(matchesTab, false);

				// We undo the move
				matchesTab = clone(matchesTabCopy);

				// We update the best score and best move if the current score is better
				if (score > bestScore) {
					bestScore = score;
					bestmove[0] = legalLines[i];
					bestmove[1] = legalNumberOfMatches[j];
				}

			}
		}

		return bestmove;

	}

	/**
	 * Returns the score of the current move
	 * @param matchesTab The matches table
	 * @param isMaximizing True if it is the computer's turn
	 * @return The score of the current move
	 */
	int miniMax(int[] matchesTab, boolean isMaximizing) {
		
		// We create a copy of the matches table
		int[] matchesTabCopy = clone(matchesTab);

		// We initialize the score to 0
		int bestScore = 0;

		// We set the current player to the computer if it is maximizing
		String currentPlayer = "CPU";

		if (!isMaximizing) {
			// We set the current player to the player if it is minimizing
			currentPlayer = "Player";
		}
		
		// If the current player has won then we return the score
		if (hasComputerWon(matchesTab, currentPlayer)) {
			bestScore = 100;
		} else if (hasPlayerWon(matchesTab, currentPlayer)) {
			bestScore = -100;
		} else {

			if (isMaximizing) {

				// We set the best score to -1000
				bestScore = -1000;

				// We get the list of the legal lines
				int[] legalLines = getListLegalLines(matchesTab);

				// We loop through the legal lines
				for (int i = 0; i < legalLines.length; i++) {

					// We get the list of the legal number of matches for the current line
					int[] legalNumberOfMatches = getListLegalNumberOfMatches(matchesTab, legalLines[i]);

					// We loop through the legal number of matches for line i
					for (int j = 0; j < legalNumberOfMatches.length; j++) {

						// We update the matches table with the current move
						updateMatches(matchesTab, legalLines[i], legalNumberOfMatches[j]);

						// We recursively get the score of the current move
						int score = miniMax(matchesTab, false);

						// We undo the move
						matchesTab = clone(matchesTabCopy);

						// We update the best score if the current score is better
						if (score > bestScore) {
							bestScore = score;
						}
					}
				}

			} else {

				// We set the best score to 1000
				bestScore = 1000;

				// We get the list of the legal lines
				int[] legalLines = getListLegalLines(matchesTab);

				// We loop through the legal lines
				for (int i = 0; i < legalLines.length; i++) {

					// We get the list of the legal number of matches for the current line
					int[] legalNumberOfMatches = getListLegalNumberOfMatches(matchesTab, legalLines[i]);

					// We loop through the legal number of matches for line i
					for (int j = 0; j < legalNumberOfMatches.length; j++) {

						// We update the matches table with the current move
						updateMatches(matchesTab, legalLines[i], legalNumberOfMatches[j]);

						// We recursively get the score of the current move
						int score = miniMax(matchesTab, true);

						// We undo the move
						matchesTab = clone(matchesTabCopy);

						// We update the best score if the current score is lower
						if (score < bestScore) {
							bestScore = score;
						}
					}
				}

			}
		}

		return bestScore;
	}

	/**
	 * Tells if the computer has won
	 * @param matchesTab The matches table
	 * @param currentPlayer The current player
	 * @return True if the computer has won
	 */
	boolean hasComputerWon(int[] matchesTab, String currentPlayer) {
		return isGameOver(matchesTab) && (currentPlayer == "CPU");
	}

	/**
	 * Tells if the player has won
	 * @param matchesTab The matches table
	 * @param currentPlayer The current player
	 * @return True if the player has won
	 */
	boolean hasPlayerWon(int[] matchesTab, String currentPlayer) {
		return isGameOver(matchesTab) && (currentPlayer != "CPU");
	}

	/**
	 * Returns a table with the legal lines indexes
	 * @param matchesTab The matches table
	 * @return A list of the legal lines
	 */
	int[] getListLegalLines(int[] matchesTab) {

		// We initialize the list of legal lines
		int[] legalLines = new int[matchesTab.length];
		// We initialize the number of legal lines to 0
		int numberOfLegalLines = 0;

		// We loop through the matches table	
		for (int i = 0; i < matchesTab.length; i++) {
			// We check if the current line is legal
			if (isLegalLine(matchesTab, i)) {
				// We add the current line to the list of legal lines
				legalLines[numberOfLegalLines] = i;
				numberOfLegalLines++;
			}
		}

		// We create a new array with the correct size
		int[] legalLinesTrimmed = new int[numberOfLegalLines];

		// We copy the legal lines to the new array
		for (int i = 0; i < numberOfLegalLines; i++) {
			legalLinesTrimmed[i] = legalLines[i];
		}

		return legalLinesTrimmed;
	}

	/**
	 * Returns a table with the legal number of matches for a given line
	 * @param matchesTab The matches table
	 * @param line The line
	 * @return A list of the legal number of matches for the given line
	 */
	int[] getListLegalNumberOfMatches(int[] matchesTab, int line) {

		if (matchesTab.length == 0) {
			return new int[0];
		}

		// We initialize the list of legal number of matches
		int[] legalNumberOfMatches = new int[matchesTab[line]];
		// We initialize the number of legal number of matches to 0
		int numberOfLegalNumberOfMatches = 0;

		// We loop through the matches table
		for (int i = 1; i <= matchesTab[line]; i++) {
			// We check if the current number of matches is legal
			if (isLegalNumberofMatches(matchesTab, line, i)) {
				// We add the current number of matches to the list of legal number of matches
				legalNumberOfMatches[numberOfLegalNumberOfMatches] = i;
				numberOfLegalNumberOfMatches++;
			}
		}

		// We create a new array with the correct size
		int[] legalNumberOfMatchesTrimmed = new int[numberOfLegalNumberOfMatches];

		// We copy the legal number of matches to the new array
		for (int i = 0; i < numberOfLegalNumberOfMatches; i++) {
			legalNumberOfMatchesTrimmed[i] = legalNumberOfMatches[i];
		}

		return legalNumberOfMatchesTrimmed;
	}

	/**
	 * Clones an int array
	 * @param tab The array to copy
	 * @return A copy of the array
	 */
	int[] clone(int[] tab) {

		// We create a new array with the same size as the original array
		int[] res = new int[tab.length];

		// We loop through the original array
		for (int i = 0; i < tab.length; i++) {
			// We copy the current element
			res[i] = tab[i];
		}

		return res;
	}

	/**
	 * Asks the user who plays first and returns the the other player
	 * @param player The player's name
	 * @param computer The computer's name
	 * @return The name of the second player
	 */
	String getSecondPlayer(String player, String computer) {

		// We initialize the result to be the computer
		String res = computer;

		int choice = -1;
		// We loop until the user enters a valid input
		while (choice < 0 || choice > 1) {
			choice = SimpleInput.getInt("First to play :" + 
			  "\n0. The computer\n1. The player\n> ");
		}

		// We reverse the choice
		if (choice == 0) {
			res = player;
		}

		return res;
	}

	/**
	 * Switches the player
	 * @param currentPlayer Current player
	 * @param player1 First player
	 * @param player2 Second player
	 * @return Next player
	 */
	String switchPlayer(String currentPlayer, String player1, String player2) {

		// If the current player is player1 then we return player2
		String res = player1;
		if (currentPlayer == player1) {
			res = player2;
		}
		return res;
	}

	/**
	 * Displays the matches
	 * @param matchesTab Array of matches
	 */
	void displayMatches(int[] matchesTab) {
		System.out.println();

		// We get the number of lines
		int lines = getNumberOfActiveLines(matchesTab);
		
		// We loop through the lines
		for (int i = 0; i < lines; i++) {;
			System.out.print(i + " :");

			// We loop through the matches
			for (int j = 0; j < matchesTab[i]; j++) {

				// We display the match
				System.out.print(" | ");
			}

			System.out.println();
		}

		System.out.println();
	}

	/**
	 * Returns the number of legal lines
	 * @param matchesTab Array of matches
	 * @return Number of legal lines
	 */
	int getNumberOfLegalLines(int[] matchesTab) {
		int res = 0;
		int i = 0;

		// We loop through each line
		while (i < matchesTab.length) {

			// We check if the line can be played
			if (isLegalLine(matchesTab, i)) {

				// If it can we increment by one the res variable
				res++;
			}
			i++;
		}

		return res;
	}


	boolean isLegalLine(int[] matchesTab, int line) {
		boolean res = true;

		// We check if the chosen line is between 0 and the number of
		// active lines both included
		if (line >= getNumberOfActiveLines(matchesTab) || line < 0) {
			res = false;
		// We check if there are at least 3 matches on the line
		} else if (matchesTab[line] < 3) {
			res = false;
		}

		return res;
	}

	/**
	 * Returns the first legal line if there are multiple
	 * and -1 if there are none
	 * @param matchesTab Array of matches
	 * @return Only legal line
	 */
	int getOnlyLegalLine(int[] matchesTab) {

		// We initialize res at -1 in case there isn't any legal line
		int res = -1;
		boolean stop = false;
		int i = 0;
		
		// We loop through the lines and we stop if we find a legal one
		while (i < matchesTab.length && !stop) {

			// We check if the current line can be played
			if (isLegalLine(matchesTab, i)) {

				// If it can we assign its index to the res variable
				res = i;
				// And we stop the loop
				stop = true;
			}
			i++;
		}

		return res;
	}

	/**
	 * Returns the number of active lines
	 * @param matchesTab Array of matches
	 * @return Number of active lines
	 */
	int getNumberOfActiveLines(int[] matchesTab) {
		int res = 0;
		boolean stop = false;
		int i = 0;

		// We loop through the lines and we stop if we find an empty one
		// as there can't be an empty line between two active ones
		while (i < matchesTab.length && !stop) {

			// We check if the line isn't empty
			if (matchesTab[i] != 0) {
				res++;

			// Otherwise we stop the loop
			} else {
				stop = true;
			}
			i++;
		}
		return res;
	}

	/**
	 * Checks if the number of matches to play is legal
	 * @param matchesTab Array of matches
	 * @param line Line to play with
	 * @param matches Number of matches to play with
	 * @return True if the number of matches to play is legal
	 */
	boolean isLegalNumberofMatches(int[] matchesTab, int line, int matches) {
		boolean res = true;
		// We get the number of matches on the chosen line
		int nbMatchesOnLIne = matchesTab[line];

		// We check if the chosen number of matches is between 1 
		// and the number of matches on the line both included
		if (matches >= nbMatchesOnLIne || matches < 1) {
			res = false;

		// We check if the number of matches on the line is a multiple of 2
		} else if (nbMatchesOnLIne % 2 == 0) {
			// We check if the chosen number of matches is half of
			// the number of matches on the line
			if (nbMatchesOnLIne - matches == nbMatchesOnLIne / 2) {
				res = false;
			}
		}

		return res;
	}

	/**
	 * Updates the matches table
	 * @param matchesTab Array of matches
	 * @param line Line to play with
	 * @param matches Number of matches to play with
	 */
	void updateMatches(int[] matchesTab, int line, int matches) {

		// We save the number of matches on the chosen line
		int temp = matchesTab[line];

		// We update the number of matches on the chosen line
		matchesTab[line] = matches;

		// We put the difference between the number of matches on the last line
		matchesTab[getNumberOfActiveLines(matchesTab)] = temp - matches;
	}

		// !! TEST METHODS !!

	/**
	 * Tests all methods
	 */
	void testAll() {
		testPvpGame();
		testPvcGame();
		testIsGameOver();
		testPlayerPlay();
		testComputerPlayRandom();
		testSwitchPlayer();
		testDisplayMatches();
		testGetNumberOfLegalLines();
		testIsLegalLine();
		testGetOnlyLegalLine();
		testGetNumberOfActiveLines();
		testIsLegalNumberofMatches();
		testUpdateMatches();
		testHasComputerWon();
		testHasPlayerWon();
		testGetListLegalLines();
		testGetListLegalNumberOfMatches();
		testClone();
		testGetSecondPlayer();
	}

	/**
	 * Tests the pvpGame() method
	 */
	void testPvpGame() {
		System.out.println("\n*** testPvpGame() ***");

		System.out.println("\nEnter any name you want for the first player.");
		System.out.println("Enter any name you want for the second player" +
		  " as long as it is different from the first's.");
		System.out.println("Enter 1 when asked for the number of matches to play with.");
		System.out.println("The first player should win.\n");

		pvpGame(new int[] {3, 0, 0});

		System.out.println();
	}

	/**
	 * Tests the pvcGame() method
	 */
	void testPvcGame() {
		System.out.println("\n*** testPvcGame() ***");

		System.out.println("\nEnter any name you want for the player except for \"CPU\".");
		System.out.println("Choose the computer as the first player.");
		System.out.println("Choose the tactics you want for the CPU.");
		System.out.println("The computer should win.\n");

		pvcGame(new int[] {3, 0, 0});

		System.out.println();
	}

	/**
	 * Tests the isGameOver() method
	 */
	void testIsGameOver() {
		System.out.print("\n*** testIsGameOver() ***");

		testCaseIsGameOver(new int[] {4, 2}, false);
		testCaseIsGameOver(new int[] {4, 0}, false);
		testCaseIsGameOver(new int[] {0, 0}, true);
		testCaseIsGameOver(new int[] {}, true);

		System.out.println();
	}

	/**
	 * Tests the playerPlay() method
	 */
	void testPlayerPlay() {
		System.out.println("\n*** testPlayerPlay() ***");

		int[] matchesTab = {4, 2, 0, 0, 0, 0};

		System.out.println("\nInput 3 when asked how many matches to play on line 0.");
		System.out.println("The number of matches on line 0 should be 3.");
		System.out.println("The number of matches on line 1 should be 2.");
		System.out.println("The number of matches on line 2 should be 1.");

		playerPlay(matchesTab);

		System.out.println("Result : \n");

		displayMatches(matchesTab);

		System.out.println();
	}

	/**
	 * Tests the computerPlayRandom() method
	 */
	void testComputerPlayRandom() {
		System.out.println("\n*** testComputerPlayRandom() ***");

		int[] matchesTab = {4, 2, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		int[] matchesTabCopy = new int[matchesTab.length];
		for (int i = 0; i < matchesTab.length; i++) {
			matchesTabCopy[i] = matchesTab[i];
		}

		System.out.println("\nBase : ");
		displayMatches(matchesTab);

		System.out.println("\nThe computer should play randomly.");

		for (int i = 0; i < 5; i++) {
			computerPlayRandom(matchesTab);
			displayMatches(matchesTab);
			matchesTab = matchesTabCopy;
		}

		System.out.println();
	}

	/**
	 * Tests a call to the isGameOver() method
	 * @param matchesTab Array of matches
	 * @param expected The expected result
	 */
	void testCaseIsGameOver(int[] matchesTab, boolean expected) {
		// Act
		boolean res = isGameOver(matchesTab);

		// Arrange
		System.out.print("\nisGameOver(" + Arrays.toString(matchesTab) + ") = " + res);

		// Assert
		if (res == expected) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	/**
	 * Tests the switchPlayer() method
	 */
	void testSwitchPlayer() {
		System.out.println("\n*** testSwitchPlayer() ***");

		testCaseSwitchPlayer("current", "current", "other", "other");
		testCaseSwitchPlayer("current", "other", "current", "other");
		testCaseSwitchPlayer("current", "current", "current", "current");
		testCaseSwitchPlayer("", "other", "", "other");
		testCaseSwitchPlayer("", "", "", "");

		System.out.println();
	}

	/**
	 * Tests a call to the switchPlayer() method
	 * @param player1 First player
	 * @param player2 Second player
	 * @param currentPlayer Current player
	 * @param expected Expected result
	 */
	void testCaseSwitchPlayer(String currentPlayer, String player1,
	  String player2, String expected) {

		// Act
		String res = switchPlayer(currentPlayer, player1, player2);

		// Arrange
		System.out.print("\nswitchPlayer(\"" + currentPlayer + "\", \"" +
		  player1 + "\", \"" + player2 + "\") = " + res);

		// Assert
		if (res == expected) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	/**
	 * Tests the displayMatches() method
	 */
	void testDisplayMatches() {
		System.out.println("\n*** testDisplayMatches() ***");

		int[] matchesTab1 = {1, 2, 3, 0};
		// Should display 3 lines with respectively 1, 2 and 3 matches
		testCaseDisplayMatches(matchesTab1);

		int[] matchesTab2 = {0, 2, 3, 0};
		// Shouldn't display anything because the first line is empty
		testCaseDisplayMatches(matchesTab2);

		int[] matchesTab3 = {1, 2, 3, 1};
		// Should display 4 lines with respectively 1, 2, 3 and 1 matches
		testCaseDisplayMatches(matchesTab3);

		int[] matchesTab4 = {};
		// Shouldn't display anything because the array is empty
		testCaseDisplayMatches(matchesTab4);

		System.out.println();
	}

	/**
	 * Tests a call to the displayMatches() method
	 * @param matchesTab Array of matches
	 */
	void testCaseDisplayMatches(int[] matchesTab) {

		// Arrange
		System.out.print("\ndisplayMatches(" + Arrays.toString(matchesTab) +
		  ") =\n\t\t");

		// Act
		displayMatches(matchesTab);

	}

	/**
	 * Tests the getNumberOfLegalLines() method
	 */
	void testGetNumberOfLegalLines() {
		System.out.print("\n*** testGetNumberOfLegalLines() ***");

		int[] matchesTab1 = {5, 3, 2, 1, 0};
		testCaseGetNumberOfLegalLines(matchesTab1, 2);

		int[] matchesTab2 = {1, 1, 2, 1, 4};
		testCaseGetNumberOfLegalLines(matchesTab2, 1);

		int[] matchesTab3 = {2, 1, 2, 1, 1};
		testCaseGetNumberOfLegalLines(matchesTab3, 0);

		int[] matchesTab4 = {0, 0, 0, 0, 0};
		testCaseGetNumberOfLegalLines(matchesTab4, 0);

		System.out.println();
	}

	/**
	 * Tests a call to the getNumberOfLegalLines() method
	 * @param matchesTab Array of matches
	 * @param expected Expected result
	 */
	void testCaseGetNumberOfLegalLines(int[] matchesTab, int expected) {

		// Act
		int res = getNumberOfLegalLines(matchesTab);

		// Arrange
		System.out.print("\ngetNumberOfLegalLines(" +
		  Arrays.toString(matchesTab) + ") = " + res);

		// Assert
		if (res == expected) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	/**
	 * Tests the isLegalLine() method
	 */
	void testIsLegalLine() {
		System.out.print("\n*** testIsLegalLine() ***");

		int[] matchesTab1 = {5, 3, 2, 1, 0};
		testCaseIsLegalLine(matchesTab1, 0, true);
		testCaseIsLegalLine(matchesTab1, 1, true);
		testCaseIsLegalLine(matchesTab1, 2, false);
		testCaseIsLegalLine(matchesTab1, 3, false);
		testCaseIsLegalLine(matchesTab1, 4, false);

		int[] matchesTab2 = {1, 1, 2, 1, 4};
		testCaseIsLegalLine(matchesTab2, 0, false);
		testCaseIsLegalLine(matchesTab2, 1, false);
		testCaseIsLegalLine(matchesTab2, 2, false);
		testCaseIsLegalLine(matchesTab2, 3, false);
		testCaseIsLegalLine(matchesTab2, 4, true);

		int[] matchesTab3 = {2, 1, 2, 1, 1};
		testCaseIsLegalLine(matchesTab3, 0, false);
		testCaseIsLegalLine(matchesTab3, 1, false);
		testCaseIsLegalLine(matchesTab3, 2, false);
		testCaseIsLegalLine(matchesTab3, 3, false);
		testCaseIsLegalLine(matchesTab3, 4, false);

		System.out.println();
	}

	/**
	 * Tests a call to the isLegalLine() method
	 * @param matchesTab Array of matches
	 * @param line Line number
	 * @param expected Expected result
	 */
	void testCaseIsLegalLine(int[] matchesTab, int line, boolean expected) {

		// Act
		boolean res = isLegalLine(matchesTab, line);

		// Arrange
		System.out.print("\nisLegalLine(" + Arrays.toString(matchesTab) +
		  ", " + line + ") = " + res);

		// Assert
		if (res == expected) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	/**
	 * Tests the getOnlyLegalLine() method
	 */
	void testGetOnlyLegalLine() {
		System.out.print("\n*** testGetOnlyLegalLine() ***");

		int[] matchesTab1 = {5, 2, 2, 1, 0};
		testCaseGetOnlyLegalLine(matchesTab1, 0);

		int[] matchesTab2 = {1, 1, 2, 1, 4};
		testCaseGetOnlyLegalLine(matchesTab2, 4);

		int[] matchesTab3 = {2, 1, 2, 1, 1};
		testCaseGetOnlyLegalLine(matchesTab3, -1);

		int[] matchesTab4 = {2, 3, 2, 4, 1};
		testCaseGetOnlyLegalLine(matchesTab4, 1);

		System.out.println();
	}

	/**
	 * Tests a call to the getOnlyLegalLine() method
	 * @param matchesTab Array of matches
	 * @param expected Expected result
	 */
	void testCaseGetOnlyLegalLine(int[] matchesTab, int expected) {

		// Act
		int res = getOnlyLegalLine(matchesTab);

		// Arrange
		System.out.print("\ngetOnlyLegalLine(" +
		  Arrays.toString(matchesTab) + ") = " + res);

		// Assert
		if (res == expected) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	/**
	 * Tests the getNumberOfMatches() method
	 */
	void testGetNumberOfActiveLines() {
		System.out.print("\n*** testGetNumberOfActiveLines() ***");

		int[] matchesTab1 = {5, 3, 2, 1, 0};
		testCaseGetNumberOfActiveLines(matchesTab1, 4);

		int[] matchesTab2 = {1, 1, 2, 1, 4};
		testCaseGetNumberOfActiveLines(matchesTab2, 5);

		int[] matchesTab3 = {2, 1, 2, 1, 1};
		testCaseGetNumberOfActiveLines(matchesTab3, 5);

		int[] matchesTab4 = {0, 0};
		testCaseGetNumberOfActiveLines(matchesTab4, 0);

		int[] matchesTab5 = {};
		testCaseGetNumberOfActiveLines(matchesTab5, 0);

		System.out.println();
	}

	/**
	 * Tests a call to the getNumberOfActiveLines() method
	 * @param matchesTab Array of matches
	 * @param expected Expected result
	 */
	void testCaseGetNumberOfActiveLines(int[] matchesTab, int expected) {

		// Act
		int res = getNumberOfActiveLines(matchesTab);

		// Arrange
		System.out.print("\ngetNumberOfActiveLines(" +
		  Arrays.toString(matchesTab) + ") = " + res);

		// Assert
		if (res == expected) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	/**
	 * Tests the getNumberOfMatches() method
	 */
	void testIsLegalNumberofMatches() {
		System.out.print("\n*** testIsLegalNumberofMatches() ***");

		int[] matchesTab1 = {5, 3, 2, 1, 0};
		testCaseIsLegalNumberofMatches(matchesTab1, 0, 2, true);
		testCaseIsLegalNumberofMatches(matchesTab1, 2, 2, false);
		testCaseIsLegalNumberofMatches(matchesTab1, 1, 3, false);


		int[] matchesTab2 = {1, 1, 2, 1, 4};
		testCaseIsLegalNumberofMatches(matchesTab2, 0, 1, false);
		testCaseIsLegalNumberofMatches(matchesTab2, 0, 2, false);
		testCaseIsLegalNumberofMatches(matchesTab2, 4, 2, false);


		System.out.println();
	}
	
	/**
	 * Tests a call to the isLegalNumberofMatches() method
	 * @param matchesTab Array of matches
	 * @param line Line number
	 * @param numberOfMatches Number of matches
	 * @param expected Expected result
	 */
	void testCaseIsLegalNumberofMatches(int[] matchesTab, int line,
	  int numberOfMatches, boolean expected) {

		// Act
		boolean res = isLegalNumberofMatches(matchesTab, line, numberOfMatches);

		// Arrange
		System.out.print("\nisLegalNumberofMatches(" +
		  Arrays.toString(matchesTab) + ", " + line + ", " + numberOfMatches +
		  ") = " + res);

		// Assert
		if (res == expected) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	/**
	 * Tests the getNumberOfMatches() method
	 */
	void testUpdateMatches() {
		System.out.print("\n*** testUpdateMatches() ***");

		int[] matchesTab1 = {5, 3, 2, 1, 0};
		testCaseUpdateMatches(matchesTab1, 0, 2, new int[] {2, 3, 2, 1, 3});
		
		int[] matchesTab2 = {1, 1, 2, 1, 4, 0};
		testCaseUpdateMatches(matchesTab2, 4, 1, new int[] {1, 1, 2, 1, 1, 3});

		int[] matchesTab3 = {2, 3, 2, 0};
		testCaseUpdateMatches(matchesTab3, 1, 1, new int[] {2, 1, 2, 2});

		System.out.println();
	}

	/**
	 * Tests a call to the updateMatches() method
	 * @param matchesTab Array of matches
	 * @param line Line number
	 * @param numberOfMatches Number of matches
	 * @param expected Expected result
	 */
	void testCaseUpdateMatches(int[] matchesTab, int line,
	  int numberOfMatches, int[] expected) {

		// Act
		updateMatches(matchesTab, line, numberOfMatches);

		// Arrange
		System.out.print("\nupdateMatches(" + Arrays.toString(matchesTab) +
		  ", " + line + ", " + numberOfMatches + ") = " +
		  Arrays.toString(matchesTab));

		// Assert
		if (Arrays.equals(matchesTab, expected)) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	/**
	 * Tests the hasComputerWon() method
	 */
	void testHasComputerWon() {
		System.out.print("\n*** testHasCompuetWon() ***");

		int[] matchesTab1 = {3, 3, 2, 1, 0, 0, 0, 0, 0};
		testCaseHasComputerWon(matchesTab1, "CPU", false);

		int[] matchesTab2 = {1, 1, 2, 1, 4, 0, 0, 0, 0};
		testCaseHasComputerWon(matchesTab2, "Player", false);

		int[] matchesTab3 = {2, 1, 2, 2};
		testCaseHasComputerWon(matchesTab3, "CPU", true);

		int[] matchesTab4 = {0, 0, 0, 0};
		testCaseHasComputerWon(matchesTab4, "Player", false);

		System.out.println();
	}

	/**
	 * Tests a call to the hasComputerWon() method
	 * @param matchesTab Array of matches
	 * @param currentPlayer Current player
	 * @param expected Expected result
	 */
	void testCaseHasComputerWon(int[] matchesTab, String currentPlayer, boolean expected) {
		// Act
		boolean res = hasComputerWon(matchesTab, currentPlayer);

		// Arrange
		System.out.print("\nhasComputerWon(" + Arrays.toString(matchesTab) +
		  ", " + currentPlayer + ") = " + res);

		// Assert
		if (res == expected) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	/**
	 * Tests the hasPlayerWon() method
	 */
	void testHasPlayerWon() {
		System.out.print("\n*** testHasCompuetWon() ***");

		int[] matchesTab1 = {3, 3, 2, 1, 0, 0, 0, 0, 0};
		testCaseHasPlayerWon(matchesTab1, "CPU", false);

		int[] matchesTab2 = {1, 1, 2, 1, 4, 0, 0, 0, 0};
		testCaseHasPlayerWon(matchesTab2, "Player", false);

		int[] matchesTab3 = {2, 1, 2, 2};
		testCaseHasPlayerWon(matchesTab3, "Player", true);

		int[] matchesTab4 = {0, 0, 0, 0};
		testCaseHasPlayerWon(matchesTab4, "CPU", false);

		System.out.println();
	}

	/**
	 * Tests a call to the hasPlayerWon() method
	 * @param matchesTab Array of matches
	 * @param currentPlayer Current player
	 * @param expected Expected result
	 */
	void testCaseHasPlayerWon(int[] matchesTab, String currentPlayer, boolean expected) {
		// Act
		boolean res = hasPlayerWon(matchesTab, currentPlayer);

		// Arrange
		System.out.print("\nhasPlayerWon(" + Arrays.toString(matchesTab) +
		  ", " + currentPlayer + ") = " + res);

		// Assert
		if (res == expected) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	/**
	 * Tests the getListLegalLines() method
	 */
	void testGetListLegalLines() {
		System.out.print("\n*** testGetListLegalLines() ***");

		int[] matchesTab1 = {3, 3, 2, 1, 0, 0, 0, 0, 0};
		testCaseGetListLegalLines(matchesTab1, new int[] {0, 1});

		int[] matchesTab2 = {1, 1, 2, 1, 4, 0, 0, 0, 0};
		testCaseGetListLegalLines(matchesTab2, new int[] {4});

		int[] matchesTab3 = {2, 1, 2, 2};
		testCaseGetListLegalLines(matchesTab3, new int[] {});

		int[] matchesTab4 = {};
		testCaseGetListLegalLines(matchesTab4, new int[] {});

		System.out.println();
	}

	/**
	 * Tests a call to the getListLegalLines() method
	 * @param matchesTab Array of matches
	 * @param expected Expected result
	 */
	void testCaseGetListLegalLines(int[] matchesTab, int[] expected) {
		// Act
		int[] res = getListLegalLines(matchesTab);

		// Arrange
		System.out.print("\ngetListLegalLines(" + Arrays.toString(matchesTab) +
		  ") = " + Arrays.toString(res));

		// Assert
		if (Arrays.equals(res, expected)) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	/**
	 * Tests the getListLegalNumberOfMatches() method
	 */
	void testGetListLegalNumberOfMatches() {
		System.out.print("\n*** testGetListLegalNumberOfMatches() ***");

		int[] matchesTab1 = {3, 3, 2, 1, 0, 0, 0, 0, 0};
		testCaseGetListLegalNumberOfMatches(matchesTab1, 0, new int[] {1, 2});
		testCaseGetListLegalNumberOfMatches(matchesTab1, 1, new int[] {1, 2});

		int[] matchesTab2 = {1, 1, 2, 1, 4, 0, 0, 0, 0};
		testCaseGetListLegalNumberOfMatches(matchesTab2, 4, new int[] {1, 3});

		int[] matchesTab3 = {2, 1, 2, 2};
		testCaseGetListLegalNumberOfMatches(matchesTab3, 1, new int[] {});

		int[] matchesTab4 = {};
		testCaseGetListLegalNumberOfMatches(matchesTab4, 0, new int[] {});

		System.out.println();
	}

	/**
	 * Tests a call to the getListLegalNumberOfMatches() method
	 * @param matchesTab Array of matches
	 * @param line Line number
	 * @param expected Expected result
	 */
	void testCaseGetListLegalNumberOfMatches(int[] matchesTab, int line, int[] expected) {
		// Act
		int[] res = getListLegalNumberOfMatches(matchesTab, line);

		// Arrange
		System.out.print("\ngetListLegalNumberOfMatches(" + Arrays.toString(matchesTab) +
		  ", " + line + ") = " + Arrays.toString(res));

		// Assert
		if (Arrays.equals(res, expected)) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	void testClone() {
		System.out.print("\n*** testClone() ***");

		int[] matchesTab1 = {3, 3, 2, 1, 0, 0, 0, 0, 0};
		testCaseClone(matchesTab1, new int[] {3, 3, 2, 1, 0, 0, 0, 0, 0});

		int[] matchesTab2 = {1, 1, 2, 1, 4, 0, 0, 0, 0};
		testCaseClone(matchesTab2, new int[] {1, 1, 2, 1, 4, 0, 0, 0, 0});

		int[] matchesTab3 = {2, 1, 2, 2};
		testCaseClone(matchesTab3, new int[] {2, 1, 2, 2});

		int[] matchesTab4 = {};
		testCaseClone(matchesTab4, new int[] {});

		System.out.println();
	}
		
	void testCaseClone(int[] matchesTab, int[] expected) {
		// Act
		int[] res = clone(matchesTab);

		// Arrange
		System.out.print("\nclone(" + Arrays.toString(matchesTab) +
			") = " + Arrays.toString(res));

		// Assert
		if (Arrays.equals(res, expected)) {
			System.out.print(" : OK");
		} else {
			System.out.print(" : ERROR");
		}
	}

	/**
	 * Tests the getSecondPLayer() method
	 */
	void testGetSecondPlayer() {
		System.out.println("\n*** testGetSecondPlayer() ***");

		System.out.println("Choose the player you want and the other one should be displayed.");

		System.out.println("Result : " + getSecondPlayer("Player", "CPU"));		

		System.out.println();
	}

}
